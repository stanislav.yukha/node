const fs = require('fs');
const path = require('path');
const bp = require('body-parser');
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');


const app = express();
const port = 8080;
const accessLogStream = fs.createWriteStream(path.join(__dirname, 'logs.log'), { flags: 'a' });
app.use(cors());
app.use(bp.json());
app.use(morgan('combined', { stream: accessLogStream }));
app.use(morgan('tiny'));


app.post('/api/files', (req, res) => createFile(req, res));

app.get('/api/files', (req, res) => getFiles(res))

app.get('/api/files/:filename', (req, res) => getFile(req, res));

app.patch('/api/files/:filename', (req, res) => updateFile(req, res));

app.delete('/api/files/:filename', (req, res) => deleteFile(req, res));
 
app.listen(port, () => {
   createFolder();
   console.log(`Server is running on ${port} port`);
})


function createFile(req, res) {
   const name = req.body.filename;
   const content = req.body.content;
   const password = req.body.password;
   const extensionList = /^.(log|txt|json|yaml|xml|js)$/;
   const filePath = path.join(__dirname,"files",`${name}`);

   if (name === undefined) {
      return res.status(400).json({message: 'Please specify \'filename\' parameter'});
   } else if (!extensionList.test(name.slice(name.lastIndexOf('.')))) {
      return res.status(400).json({message: 'Bad file extension'});
   }
   if (content === undefined) {
      return res.status(400).json({message: 'Please specify \'content\' parameter'});
   }
   if (password === '') {
      return res.status(400).json({message: 'Password paramater should not be an empty string'});
   }
   if (fs.existsSync(filePath)) {
      return res.status(400).json({message: 'Error: file already exists'});
   } else {
      fs.writeFile(filePath, content, function (err) {
         if (err) {
            return res.status(500).json({message: 'Server error'});
         }
         if(password) {
            addPassword(name, password);
         } 
         return res.status(200).json({message: 'File created successfully'});
         });
     
   }
}

function getFiles(res) {
   const fileList = [];
   fs.readdir(path.join(__dirname,'files'), (err, files) => {
      if(err) {
         return res.status(500).json({message: 'Server error'});
      }
      files.forEach(file => {
         fileList.push(file);
      });
      if (fileList.length) {
         return res.status(200).json({
            message: 'Success',
            files: fileList
         })
      } else {
         return res.status(400).json({message: 'No files found'});
      }
   })
}

function getFile(req, res) {
   const name = req.params.filename;
   const filepass = req.query.password;
   const filePath = path.join(__dirname, 'files', `${name}`);

   if(!fs.existsSync(filePath)) {
      return res.status(400).json({message: `No file with '${name}' filename found`});
   }
   if (checkIfProtected(name)) {
      if(filepass === undefined) {
         return res.status(400).json({message: 'Please specify \'password\' parameter for protected file'});
      } else if (!checkPasswordMatch(name, filepass)) {
         return res.status(400).json({message: 'Wrong password provided for protected file'});
      } 
   }
   const fileExtension = name.slice(name.lastIndexOf('.') + 1);
   try {
      const fileContent = fs.readFileSync(filePath, {encoding: 'utf-8'});
      const fileUploadDate = fs.statSync(filePath).atime;
      return res.status(200).json(
         {
            message: "Success",
            filename: name,
            content: fileContent,
            extension: fileExtension,
            uploadedDate: fileUploadDate
         })
   } catch(err) {
      return res.status(500).json({message: 'Server error'});
   }
}

function updateFile(req, res) {
   const name = req.params.filename;
   const content = req.body.content;
   const password = req.body.password;
   const rewrite = req.body.rewrite;
   const filePath = path.join(__dirname,"files",`${name}`);

   if (name === undefined) {
      return res.status(400).json({message: 'Please specify \'filename\' parameter'});
   }
   if (content === undefined) {
      return res.status(400).json({message: 'Please specify \'content\' parameter'});
   }
   if(!fs.existsSync(filePath)) {
      return res.status(400).json({message: `No file with '${name}' filename found`});
   }
   if (checkIfProtected(name)) {
      if(password === undefined) {
         return res.status(400).json({message: 'Please specify \'password\' parameter for protected file'});
      } else if (!checkPasswordMatch(name, password)) {
         return res.status(400).json({message: 'Wrong password provided for protected file'});
      } 
   }
   if (rewrite) {
      fs.writeFile(filePath,content, function (err) {
         if (err) {
            return res.status(500).json({message: 'Server error'});
         } 
         return res.status(200).json({message: 'File rewritten successfully'});
         });
   } else {
      if (content === '') {
         return res.status(400).json({message: 'Parameter \'content\' should not be empty in append mode'});
      }
      fs.appendFile(filePath, `\n${content}`, function (err) {
         if (err) {
            return res.status(500).json({message: 'Server error'});
         } 
         return res.status(200).json({message: 'File updated successfully'});
         });
   }
}

function deleteFile(req, res) {
   const name = req.params.filename;
   const password = req.body.password;
   const filePath = path.join(__dirname,"files",`${name}`);

   if (name === undefined) {
      return res.status(400).json({message: 'Please specify \'filename\' parameter'});
   }
   if(!fs.existsSync(filePath)) {
      return res.status(400).json({message: `No file with '${name}' filename found`});
   }
   if (checkIfProtected(name)) {
      if(password === undefined) {
         return res.status(400).json({message: 'Please specify \'password\' parameter for protected file'});
      } else if (!checkPasswordMatch(name, password)) {
         return res.status(400).json({message: 'Wrong password provided for protected file'});
      } 
   }

   try {
      fs.unlinkSync(filePath);
      if (checkIfProtected(name)) {
         removePassword(name);
      }
      return res.status(200).json({message: 'File deleted successfully'});
    } catch(err) {
      return res.status(500).json({message: 'Server error'});
    }
}

function createFolder() {
   if (!fs.existsSync(path.join(__dirname,'files'))){
      fs.mkdir(path.join(__dirname,'files'), (err) => {
         if(err) {
            console.log(err)
         }
      });
   }
}

function addPassword(filename, password) {
   let passObj = readPassFile();
   passObj[filename] = password;
   fs.writeFileSync(path.join(__dirname, 'passwords.json'), JSON.stringify(passObj));
}

function checkIfProtected(filename) {
   let passObj = readPassFile();
   return !!passObj[filename];
}

function checkPasswordMatch(filename, password) {
   let passObj = readPassFile();
   return passObj[filename] === password;
}

function removePassword(filename) {
   let passObj = readPassFile();
   delete passObj[filename];
   fs.writeFileSync(path.join(__dirname, 'passwords.json'), JSON.stringify(passObj));
}

function readPassFile() {
   let obj = {};
      if (fs.existsSync(path.join(__dirname, 'passwords.json'))) {
         obj = fs.readFileSync(path.join(__dirname, 'passwords.json'), {encoding: 'utf-8'});
         obj = JSON.parse(obj);
      }
   return obj;
}
